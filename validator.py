import os
import json
from jsonschema import validate, RefResolver
import jsonschema

schema_dir = 'sc/'
samples_dir = 'sd/'

schema_files = [f for f in os.listdir(schema_dir) if f.endswith('.json')  and  'survey' not in f]
sample_files = [f for f in os.listdir(samples_dir) if f.endswith('.json')  and  'survey' not in f]

isError = False

for sample in sample_files:
  index = sample.find('sample-data')
  schema_start = sample[0:index]

  for schema in schema_files:
    splits = schema.split('-')
    index = schema.find(splits[-1])
    if schema_start == schema[:index]:
      schema_fp = open(schema_dir+schema, 'r')
      instance_fp = open(samples_dir+sample, 'r')
      instance = json.load(instance_fp)
      schema_json = json.load(schema_fp)
      schema_path = 'file://{0}/'.format(os.path.dirname(os.path.realpath(schema_fp.name)))
      resolver = RefResolver(schema_path, schema_json)
     
      try:
        validate(instance, schema_json, resolver=resolver)
      except Exception as e:
        msg = 'Validation failed on instance {0} for schema {1}'.format(sample, schema) 
        print(msg)
        print(e)
        isError = True

      schema_fp.close()
      instance_fp.close()

if isError: exit(1)
exit(0)

